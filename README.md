#### How to run
```sh
npm i && npm start
```

:warning: This client need the following API in order to work properly https://gitlab.com/oauth2.0/oauth-server

:warning: That API has seed data, and it contains an user:
- username: luisbar
- password: huachinango

