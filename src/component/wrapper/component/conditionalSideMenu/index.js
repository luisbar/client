import React from 'react';
import PropTypes from 'prop-types';

import { SideMenu } from 'component';
import { LOGIN } from 'common/config';

export const ConditionalSideMenu = ({ pathname }) => {

  return (
    pathname !== LOGIN
    ? <SideMenu
        pathname={pathname}/>
    : null
  );
}

ConditionalSideMenu.propTypes = {
  pathname: PropTypes.string.isRequired,
};
