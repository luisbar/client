import React from 'react';
import PropTypes from 'prop-types';

import { Toolbar } from 'component';
import { LOGIN } from 'common/config';

export const ConditionalToolbar = ({ pathname }) => {

  return (
    pathname !== LOGIN
    ? <Toolbar/>
    : null
  );
}

ConditionalToolbar.propTypes = {
  pathname: PropTypes.string.isRequired,
};
