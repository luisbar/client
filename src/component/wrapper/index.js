import React, { Component } from 'react';
import { Grommet, Box } from 'grommet';
import PropTypes from 'prop-types';

import theme from 'common/theme';
import style from './style';
import { ErrorBoundary, TransitionAnimator, Router, ErrorToast } from 'component';
import { ConditionalSideMenu, ConditionalToolbar } from './component';

class Wrapper extends Component {

  render() {

    const { location } = this.props;

    return (
      <Grommet
        full
        theme={theme}>
        <ErrorBoundary>
          <Box
            fill
            direction={'row'}
            background={'dark-3'}>
            <ConditionalSideMenu
              pathname={location.pathname}/>
            <Box
              fill
              style={style().erroToastContainer}>
              <ErrorToast>
                <ConditionalToolbar
                  pathname={location.pathname}/>
                <TransitionAnimator
                  transitionKey={location.pathname}>
                  <Router
                    location={location}/>
                </TransitionAnimator>
              </ErrorToast>
            </Box>
          </Box>
        </ErrorBoundary>
      </Grommet>
    );
  }
}

Wrapper.propTypes = {
  location: PropTypes.object.isRequired,
};

export default Wrapper;
