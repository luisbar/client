import React from 'react';
import { Button } from 'grommet';
import { Close } from 'grommet-icons'

import style from './style';

export const CloseButton = (props) => (
  <Button
    {...props}
    style={style.button}/>
);

CloseButton.defaultProps = {
  icon: <Close/>,
};
