import React from 'react';
import PropTypes from 'prop-types';
import { Accordion, AccordionPanel, Paragraph } from 'grommet';

import style from './style';
import { TXT_2 } from 'common/string';

export const DetailError = (props) => {

  const {
    error,
    errorInfo,
    accordion,
    accordionPanel,
    firstParagraph,
    secondParagraph,
  } = props;

  return (
    <Accordion
      {...accordion}
      style={style.accordion}>
      <AccordionPanel
        {...accordionPanel}>
        <Paragraph
          {...firstParagraph}
          style={style.paragraph}>
          {error.toString()}
        </Paragraph>
        <Paragraph
          {...secondParagraph}
          style={style.paragraph}>
          {errorInfo.componentStack}
        </Paragraph>
      </AccordionPanel>
    </Accordion>
  );
};

DetailError.propTypes = {
  error: PropTypes.any,
  errorInfo: PropTypes.any,
  accordion: PropTypes.any,
  accordionPanel: PropTypes.any,
  firstParagraph: PropTypes.any,
  secondParagraph: PropTypes.any,
};

DetailError.defaultProps = {
  accordionPanel: {
    label: TXT_2,
  },
};
