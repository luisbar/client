import React from 'react';
import { Heading } from 'grommet';

import { TXT_1 } from 'common/string';

export const ApologizedMessage = (props) => (
  <Heading
    {...props}>
    {props.children}
  </Heading>
);

ApologizedMessage.defaultProps = {
  tag: 'h3',
  textAlign: 'center',
  children: TXT_1,
};
