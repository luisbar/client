import React, { Component } from 'react';
import { Box } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';

import { HOME } from 'common/config';
import { CloseButton, ApologizedMessage, DetailError } from './component';

class ErrorBoundary extends Component {

  state = {
    error: null,
    errorInfo: null
  };

  componentDidCatch(error, errorInfo) {
    this.setState({ error: error, errorInfo: errorInfo });
  }

  render() {

    const { error, errorInfo } = this.state;

    if (error)
      return (
        <Box
          full={true}
          justify={'center'}
          colorIndex={'neutral-1'}>
          <CloseButton
            onClick={this.onClosePressed}/>
          <ApologizedMessage/>
          <DetailError
            error={error}
            errorInfo={errorInfo}/>
        </Box>
      );

    return this.props.children;
  }

  onClosePressed = (event) => {
    this.setState({ error: null, errorInfo: null });
    this.props.goToHome();
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  goToHome: () => replace(HOME),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorBoundary));
