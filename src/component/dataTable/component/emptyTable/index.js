import React from 'react';
import { Box, Heading } from 'grommet';

export const EmptyTable = (props) => {

  return (
    <Box
      flex
      align={'center'}>
      <Heading
        size={'small'}>
        {'No hay datos'}
      </Heading>
    </Box>
  );
};
