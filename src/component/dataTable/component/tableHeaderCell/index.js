import React from 'react';
import { Box, Text } from 'grommet';

export const TableHeaderCell = ({ dataKey, label }) => {

  return (
    <Box
      align={dataKey === 'action' ? 'center' : 'start'}
      border={'bottom'}
      pad={{ bottom: 'small' }}>
      <Text
        size={'small'}>
        {label}
      </Text>
    </Box>
  );
};
