import React from 'react';
import { Box, Button, ResponsiveContext } from 'grommet';
import { Trash, Edit, View } from 'grommet-icons';

export const Actions = ({ onEdit, onView, onDelete }) => {

  return (
    <ResponsiveContext.Consumer>
    {(size) => (
      <Box
        wrap
        direction={'row'}
        justify={'center'}
        margin={'small'}>
        {
          onDelete
          ? <Button
              plain
              icon={<Trash color={'#9a34d4'}/>}
              onClick={onDelete}
              margin={'small'}/>
          : null
        }
        {
          onEdit
          ? <Button
              plain
              icon={<Edit color={'#9a34d4'}/>}
              onClick={onEdit}
              margin={'small'}/>
          : null
        }
        {
          onView
          ? <Button
              plain
              icon={<View color={'#9a34d4'}/>}
              onClick={onView}
              margin={'small'}/>
          : null
        }
      </Box>
    )}
    </ResponsiveContext.Consumer>
  );
};
