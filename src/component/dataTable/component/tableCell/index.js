import React from 'react';
import { Text } from 'grommet';

import { Actions } from './component';

export const TableCell = ({ dataKey, cellData, onEdit, onView, onDelete }) => {

  if (dataKey !== 'action')
    return (
      <Text
        size={'small'}>
        {cellData}
      </Text>
    );
  else
    return (
      <Actions
        onEdit={onEdit}
        onView={onView}
        onDelete={onDelete}/>
    );
};
