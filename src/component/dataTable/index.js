import React, { Component } from 'react';
import { Box } from 'grommet';
import { Column, Table, AutoSizer, WindowScroller } from 'react-virtualized';
import 'react-virtualized/styles.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { EmptyTable, TableHeaderCell, TableCell } from './component';
import { FloatingButton } from 'component';

export class DataTable extends Component {

  render() {

    const { data, headerLabels, floatingButtonPath } = this.props;

    return (
      <Box
        flex>
        <WindowScroller>
          {({ height, isScrolling, onChildScroll, scrollTop }) =>
            <AutoSizer>
              {({ width, height }) =>
                <Table
                  noRowsRenderer={this.onNoRowsRenderer}
                  width={width}
                  height={height}
                  headerHeight={50}
                  rowHeight={53}
                  rowCount={data.length}
                  rowGetter={this.onRenderData}>
                  {!!data.length && ['acción'].concat(headerLabels).map(this.onRenderColumn)}
                </Table>
              }
            </AutoSizer>
          }
        </WindowScroller>

        <Link
          replace
          to={floatingButtonPath}>
          <FloatingButton/>
        </Link>
      </Box>
    );
  }

  onNoRowsRenderer = () => (
    <EmptyTable/>
  );

  onRenderData = ({ index }) => (
    this.props.data[index]
  );

  onRenderColumn = (label, index) => (
    <Column
      key={index}
      label={label}
      dataKey={['action'].concat(this.props.dataKeys)[index]}
      width={Math.max(document.documentElement.clientWidth, window.innerWidth || 0)}
      headerRenderer={this.onHeaderRenderer}
      cellRenderer={this.onCellRenderer}/>
  );

  onHeaderRenderer = ({ columnData, dataKey, disableSort, label, sortBy, sortDirection }) => (
    <TableHeaderCell
      dataKey={dataKey}
      label={label}/>
  );

  onCellRenderer = ({ cellData, columnData, columnIndex, dataKey, isScrolling, rowData, rowIndex }) => {

    const { data, onEdit, onView, onDelete } = this.props;

    return (
      <TableCell
        dataKey={dataKey}
        cellData={cellData}
        onEdit={onEdit && this.onEdit(data[rowIndex].id)}
        onView={onView && this.onView(data[rowIndex].id)}
        onDelete={onDelete && this.onDelete(data[rowIndex].id)}/>
    );
  }

  onEdit = (id) => () => {
    this.props.onEdit(id);
  }

  onView = (id) => () => {
    this.props.onView(id);
  }

  onDelete = (id) => () => {
    this.props.onDelete(id);
  }
}

DataTable.propTypes = {
  data: PropTypes.array.isRequired,
  headerLabels: PropTypes.array.isRequired,
  dataKeys: PropTypes.array.isRequired,
  floatingButtonPath: PropTypes.string.isRequired,
  onEdit: PropTypes.func,
  onView: PropTypes.func,
  onDelete: PropTypes.func,
};
DataTable.defaultProps = {
  data: [],
  headerLabels: [],
  dataKeys: [],
};
