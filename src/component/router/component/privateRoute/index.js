import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { sessionService } from 'redux-react-session';
import PropTypes from 'prop-types';

import { LOGIN } from 'common/config';
import getParamsFromUrl from 'common/getParamsFromUrl';

export class PrivateRoute extends Component {

  state = {}

  static getDerivedStateFromProps(props, state) {
    const { token } = getParamsFromUrl() || {};

    if (token)
      sessionService.saveSession({ accessToken: token });

    return state;
  }

  render() {
    const { authenticated, component, ...other } = this.props;

    return (
      <Route
        {...other}
        render={this.renderComponent}
      />
    );
  }

  renderComponent = () => {
    const { authenticated, component, location } = this.props;
    const { token } = getParamsFromUrl() || {};
    
    return (
      authenticated || token
      ?
        React.createElement(component, this.props)
      :
        <Redirect
          to={LOGIN}
          from={location.pathname}/>
    );
  }
}

PrivateRoute.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
  location: PropTypes.any.isRequired,
};
