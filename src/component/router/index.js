import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';

import { Home, Login, NotFound, PrivateRoute, User, UserCreation, Scope, ScopeCreation } from './component';
import { HOME, LOGIN, USER, USER_UPSERT, SCOPE, SCOPE_CREATE } from 'common/config';

class Router extends Component {

  render() {
    const { checked, location, authenticated } = this.props;

    return (
      checked &&
      <Switch location={location}>
        <Route exact path={LOGIN} component={Login}/>
        <PrivateRoute exact path={HOME} component={Home} authenticated={authenticated} location={location}/>
        <PrivateRoute exact path={USER} component={User} authenticated={authenticated} location={location}/>
        <PrivateRoute exact path={USER_UPSERT} component={UserCreation} authenticated={authenticated} location={location}/>
        <PrivateRoute exact path={SCOPE} component={Scope} authenticated={authenticated} location={location}/>
        <PrivateRoute exact path={SCOPE_CREATE} component={ScopeCreation} authenticated={authenticated} location={location}/>
        <Route component={NotFound}/>
      </Switch>
    );
  }
}

Router.propTypes = {
  location: PropTypes.any.isRequired,
};

const mapStateToProps = (state) => ({
  checked: state
  .session
  .checked,
  authenticated: state
  .session
  .authenticated,
});

export default withRouter(connect(
  mapStateToProps,
)(Router));
