import React from 'react';
import { Box } from 'grommet';

export const Toolbar = (props) => {

  return (
    <Box
      flex={false}
      fill={'horizontal'}
      basis={'xxsmall'}
      background={'accent-3'}>
    </Box>
  );
};
