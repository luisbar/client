import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import './animation.css';

class TransitionAnimator extends Component {

  render() {

    return (
      <TransitionGroup
        component={null}>
        <CSSTransition
          key={this.props.transitionKey}
          classNames={'fade'}
          timeout={300}>
          {this.props.children}
        </CSSTransition>
      </TransitionGroup>
    );
  }
}

TransitionAnimator.propTypes = {
  transitionKey: PropTypes.string.isRequired,
};

export default TransitionAnimator;
