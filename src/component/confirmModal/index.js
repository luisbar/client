import React, { Component } from 'react';
import { Layer, Box, Text, Paragraph, Button } from 'grommet';
import PropTypes from 'prop-types';

class ConfirmModal extends Component {

  render() {

    const { title, message, onCancel, onAccept, visibility } = this.props;

    return (
      visibility
      ? <Layer>
          <Box
            margin={'medium'}>
            <Text
              weight={'bold'}>
              {title}
            </Text>
            <Paragraph>
              {message}
            </Paragraph>
            <Box
              direction={'row'}
              gap={'small'}
              justify={'end'}>
              <Button
                color={'status-error'}
                onClick={onCancel}
                label={<Text color={'dark-1'}>{'Cancelar'}</Text>}/>
              <Button
                primary
                onClick={onAccept}
                label={<Text color={'dark-1'}>{'Aceptar'}</Text>}/>
            </Box>
          </Box>
        </Layer>
      : null
    );
  }
}

ConfirmModal.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  onCancel: PropTypes.func.isRequired,
  onAccept: PropTypes.func.isRequired,
  visibility: PropTypes.bool,
};

ConfirmModal.defaultProps = {
  title: 'Title',
  message: 'message',
  visibility: false,
};

export default ConfirmModal;
