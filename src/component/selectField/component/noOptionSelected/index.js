import React from 'react';
import { Box, Text } from 'grommet';
import PropTypes from 'prop-types';

export const NoOptionSelected = ({ placeholder }) => {

  return (
    <Box
      pad={{ vertical: 'xsmall', horizontal: 'small' }}
      margin={'xsmall'}>
      <Text>
        {placeholder}
      </Text>
    </Box>
  );
}

NoOptionSelected.propTypes = {
  placeholder: PropTypes.string,
};
