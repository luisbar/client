import React from 'react';
import { Box } from 'grommet';
import PropTypes from 'prop-types';

import { OptionSelected } from './component';

export const OptionsSelected = (props) => {

  const { itemsSelected, onUnselectedItem } = props;

  return (
    <Box
      wrap
      direction={'row'}>
      {itemsSelected.map((itemSelected, index) => (
          <OptionSelected
            key={index}
            itemSelected={itemSelected}
            onUnselectedItem={onUnselectedItem}/>
        )
      )}
    </Box>
  );
}

OptionsSelected.propTypes = {
  itemsSelected: PropTypes.array,
  onUnselectedItem: PropTypes.func.isRequired,
};

OptionsSelected.defaultProps = {
  itemsSelected: [],
};
