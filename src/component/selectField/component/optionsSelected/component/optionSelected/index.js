import React from 'react';
import { Box, Text } from 'grommet';
import { FormClose } from 'grommet-icons';
import PropTypes from 'prop-types';

export const OptionSelected = (props) => {

  const { itemSelected, onUnselectedItem } = props;

  const onClick = (idUnselectedItem) => (event) => {
    event.preventDefault();
    event.stopPropagation();
    onUnselectedItem(idUnselectedItem);
  };

  return (
    <Box
      key={itemSelected}
      onClick={onClick(itemSelected)}
      align={'center'}
      direction={'row'}
      gap={'xsmall'}
      pad={{ vertical: 'xsmall', horizontal: 'small' }}
      margin={'xsmall'}
      background={'brand'}
      round={'large'}>
      <Text
        size={'small'}
        color={'white'}>
        {itemSelected.label}
      </Text>
      <Box
        background={'white'}
        round={'full'}
        margin={{ left: 'xsmall' }}>
        <FormClose
          color={'accent-1'}
          size={'small'}/>
      </Box>
    </Box>
  );
}

OptionSelected.propTypes = {
  itemSelected: PropTypes.object.isRequired,
  onUnselectedItem: PropTypes.func.isRequired,
};
