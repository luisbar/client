import React from 'react';
import { Box, Text } from 'grommet';

export const Option = ({option, index, options, state}) =>  {

  return (
    <Box
      pad={'small'}>
      <Text>
        {option.label}
      </Text>
    </Box>
  );
}
