import React, { Component } from 'react';
import { Select, FormField } from 'grommet';
import PropTypes from 'prop-types';

import { NoOptionSelected, OptionsSelected, Option } from './component';

export class SelectField extends Component {

  state = {
    itemsSelected: []
  }

  onUnselectedItem = (unselectedItem) => {
    const { itemsSelected } = this.state;
    const { onUnselectedItem } = this.props;
    const newItemsSelected = itemsSelected.filter((itemsSelected) => itemsSelected.id !== unselectedItem.id)

    this.setState({ itemsSelected: newItemsSelected });
    onUnselectedItem(unselectedItem);
  }

  onChange = ({ option }) => {
    const { onSelectedItem, onUnselectedItem } = this.props;
    const isAlreadyAnItemSelected = this.state.itemsSelected.includes(option);

    this.setState({ itemsSelected: isAlreadyAnItemSelected
      ? this.state.itemsSelected.filter((itemsSelected) => itemsSelected.id !== option.id)
      : [...this.state.itemsSelected, option]
    });

    if (isAlreadyAnItemSelected)
      onUnselectedItem(option);
    else
      onSelectedItem(option);
  }

  getValue() {

    const { itemsSelected } = this.state;
    const { placeholder } = this.props;

    return (
      itemsSelected && itemsSelected.length
      ? <OptionsSelected
          itemsSelected={itemsSelected}
          onUnselectedItem={this.onUnselectedItem}/>
      : <NoOptionSelected
          placeholder={placeholder}/>
    );
  }

  render() {

    const { formField, select, options } = this.props;

    return (
      <FormField
        {...formField}>
        <Select
          {...select}
          value={this.getValue()}
          options={options}
          onChange={this.onChange}>
          {(option, index, options, state) => (
            <Option option={option}/>
          )}
        </Select>
      </FormField>
    );
  }
}

SelectField.propTypes = {
  formField: PropTypes.object,
  select: PropTypes.object,
  options: PropTypes.array,
  placeholder: PropTypes.string,
  onSelectedItem: PropTypes.func.isRequired,
  onUnselectedItem: PropTypes.func.isRequired,
};

SelectField.defaultProps = {
  placeholder: 'Seleccionar item',
  options: [
    {
      id: 1,
      label: 'option 1'
    },
    {
      id: 2,
      label: 'option 2'
    },
    {
      id: 3,
      label: 'option 3'
    },
    {
      id: 4,
      label: 'option 4'
    },
    {
      id: 5,
      label: 'option 5'
    },
    {
      id: 6,
      label: 'option 6'
    },
    {
      id: 7,
      label: 'option 7'
    },
    {
      id: 8,
      label: 'option 8'
    },
  ],
};
