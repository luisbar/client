import React, { Component } from 'react';
import { Box } from 'grommet';
import PropTypes from 'prop-types';
import { Home, User, Lock, Code } from 'grommet-icons';

import { MenuItem, SignOutItem, AvatarItem } from './component';
import { HOME, USER, SCOPE, CLIENT } from 'common/config';
import { TXT_7, TXT_8, TXT_9, TXT_10 } from 'common/string';

export class SideMenu extends Component {

  render() {

    const { pathname } = this.props;

    return (
      <Box
        background={'dark-2'}
        fill={'vertical'}
        width={'xsmall'}
        flex={false}>
        <AvatarItem/>
        <Box
          flex
          justify={'center'}>
          <MenuItem
            pathname={pathname}
            icon={<Home color={'white'}/>}
            label={TXT_7}
            route={HOME}/>
          <MenuItem
            pathname={pathname}
            icon={<User color={'white'}/>}
            label={TXT_8}
            route={USER}/>
          <MenuItem
            pathname={pathname}
            icon={<Lock color={'white'}/>}
            label={TXT_9}
            route={SCOPE}/>
          <MenuItem
            pathname={pathname}
            icon={<Code color={'white'}/>}
            label={TXT_10}
            route={CLIENT}/>
        </Box>
        <SignOutItem/>
      </Box>
    );
  }
}

SideMenu.propTypes = {
  pathname: PropTypes.string.isRequired,
};
