import React from 'react';
import { Button, Text } from 'grommet';
import { Logout } from 'grommet-icons';
import { Link } from 'react-router-dom';

import { TXT_11 } from 'common/string';
import { LOGIN } from 'common/config';

export const SignOutItem = (props) => {

  return (
    <Link
      replace
      to={LOGIN}>
      <Button
        plain
        margin={'small'}
        icon={<Logout color={'#AEF123'}/>}
        label={
          <Text color={'light-1'} size={'small'}>{TXT_11}</Text>
        }/>
    </Link>
  );
};
