import React from 'react';
import { Box, Text, Image } from 'grommet';

import style from './style';

export const AvatarItem = (props) => {

  return (
    <Box
      align={'center'}
      margin={{ top: 'small' }}>
      <Image
        style={style().image}
        src={'https://avatars.dicebear.com/v2/gridy/lucho.svg'}/>
      <Text
        margin={{ top: 'small' }}
        size={'small'}>
        {'luisbar'}
      </Text>
    </Box>
  );
};
