import React from 'react';
import { Box, Text } from 'grommet';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import style from './style';

export const MenuItem = (props) => {

  const { icon, label, pathname, route } = props;

  return (
    <Link
      style={style().link}
      replace
      to={route}>
      <Box
        justify={'center'}
        align={'center'}
        gap={'small'}
        margin={'small'}>
        {icon}
        <Text
          size={'small'}
          color={'light-1'}
          weight={pathname === route ? 'bold' : 'normal'}>
          {label}
        </Text>
      </Box>
    </Link>
  );
}

PropTypes.propTypes = {
  icon: PropTypes.node.isRequired,
  label: PropTypes.string.isRequired,
  pathname: PropTypes.string.isRequired,
  route: PropTypes.string.isRequired,
};
