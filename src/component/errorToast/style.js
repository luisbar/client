export default (props, state) => {

  return {

    textContainer: {
      position: 'absolute',
      width: '100%',
      opacity: 0,
      transition: 'all 0.3s ease-in-out',
      zIndex: 1,
    },
    textContainerVisible: {
      opacity: 1,
    },
  };
};
