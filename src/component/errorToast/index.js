import React, { Component } from 'react';
import { Box, Text } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import style from './style';

class ErrorToast extends Component {

  state = {
    isVisible: false,
  }

  render() {

    const { errorMessage, children } = this.props;
    const { isVisible } = this.state;
    const textContainerVisible = isVisible
      ? style().textContainerVisible
      : {};

    return (
      <Box
        fill>
        <Box
          align={'center'}
          pad={'small'}
          style={{...style().textContainer, ...textContainerVisible}}
          background={'status-error'}>
          <Text
            color={'light-1'}>
            {errorMessage}
          </Text>
        </Box>
        {children}
      </Box>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.timestamp !== this.props.timestamp && !this.state.isVisible)
      this.setState({ isVisible: true });

    if (this.state.isVisible) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.setState({ isVisible: false })
      }, 2000)
    }
  }
}

const mapStateToProps = state => {

  return {
    errorMessage: state
      .root
      .middleware
      .errorCatcher
      .errorMessage,
    timestamp: state
      .root
      .middleware
      .errorCatcher
      .timestamp,
  };
};

const mapDispatchToProps = dispatch => {

  return {

  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorToast));
