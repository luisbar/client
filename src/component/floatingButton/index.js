import React from 'react';
import { Button } from 'grommet';
import { Add } from 'grommet-icons';

import style from './style';

export const FloatingButton = (props) => {

  return (
    <Button
      {...props}
      primary
      style={style().button}
      icon={<Add color={'white'} size={'medium'}/>}/>
  );
};
