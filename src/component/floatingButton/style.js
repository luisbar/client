export default (props, state) => {

  return {

    button: {
      position: 'absolute',
      bottom: 25,
      right: 25,
      borderRadius: '50%',
      background: '#d83ecf'
    },
  };
};
