import React, { Component } from 'react';
import { FormField, TextInput } from 'grommet';
import PropTypes from 'prop-types';

import { Error } from './component';

export class TextField extends Component {

  render() {

    const { formField, textInput } = this.props;

    return (
      <FormField
        {...formField}
        error={<Error errors={formField.error}/>}>
        <TextInput
          {...textInput}/>
      </FormField>
    );
  }
}

TextField.propTypes = {
  formField: PropTypes.object,
  textInput: PropTypes.object,
};
