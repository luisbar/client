import React from 'react';
import { Box, Text } from 'grommet';
import PropTypes from 'prop-types';

export const Error = ({ errors }) => {

  return (
    <Box>
      {errors.map((item, index) =>  <Text key={index}>{item}</Text>)}
    </Box>
  );
};

Error.propTypes = {
  errors: PropTypes.array,
};

Error.defaultProps = {
  errors: [],
};
