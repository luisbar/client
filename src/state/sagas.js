import { all } from 'redux-saga/effects';

import { userSaga as user } from './user';
import { scopeSaga as scope } from './scope';
import { middlewareSaga as middleware } from './middleware';

export default function* root() {
  yield all([
    user(),
    scope(),
    middleware(),
  ]);
};
