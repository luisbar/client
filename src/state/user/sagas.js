import { all } from 'redux-saga/effects';

import { fetchUsersSaga as fetchUsers } from './fetchUsers';
import { createUserSaga as createUser } from './createUser';

export default function* root() {
  yield all([
    fetchUsers(),
    createUser(),
  ]);
};
