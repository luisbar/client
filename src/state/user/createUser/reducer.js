import {
  WILL_CREATE_USER,
  CREATING_USER,
  DID_CREATE_USER,
} from 'common/actions';

const initialState = {
  status: {
    isRunning: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export const createUserReducer = (state = initialState, action) => {

  switch (action.type) {

    case WILL_CREATE_USER:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: true,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        }
      };

    case CREATING_USER:
      if (action.error)
        return {
          ...state,
          status: {
            ...state.status,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.fieldMessages,
            },
          },
        };

      return {
        ...state,
        status: {
          ...state.status,
          wasASuccess: true,
        }
      };

    case DID_CREATE_USER:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: false,
          wasAnError: false,
          wasASuccess: false,
        },
      };

    default:
      return state;
  }
};
