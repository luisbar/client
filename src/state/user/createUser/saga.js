import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { UPSERT_USER_URL } from 'common/config';
import { requester } from 'service/';
import {
  CREATE_USER,
  WILL_CREATE_USER,
  CREATING_USER,
  DID_CREATE_USER
} from 'common/actions';

function* createUser({ payload }) {
  try {
    yield put(actionBuilder(WILL_CREATE_USER));
    const response = yield requester.post(UPSERT_USER_URL, payload);
    yield put(actionBuilder(CREATING_USER, response, false));
  } catch (error) {
    yield put(actionBuilder(CREATING_USER, error, true));
  } finally {
    yield put(actionBuilder(DID_CREATE_USER));
  }
}

export function* createUserSaga() {
  yield takeEvery(CREATE_USER, createUser);
}
