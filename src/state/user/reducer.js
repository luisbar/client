import { combineReducers } from 'redux';

import { fetchUsersReducer as fetchUsers } from './fetchUsers';
import { createUserReducer as createUser } from './createUser';

export default combineReducers({
  fetchUsers,
  createUser,
});
