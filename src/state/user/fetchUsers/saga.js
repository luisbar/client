import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { FIND_ALL_USERS_URL } from 'common/config';
import { requester } from 'service/';
import {
  FETCH_USERS,
  WILL_FETCH_USERS,
  FETCHING_USERS,
  DID_FETCH_USERS
} from 'common/actions';

function* fetchUsers() {
  try {
    yield put(actionBuilder(WILL_FETCH_USERS));
    const response = yield requester.get(FIND_ALL_USERS_URL);
    yield put(actionBuilder(FETCHING_USERS, response, false));
  } catch (error) {
    yield put(actionBuilder(FETCHING_USERS, error, true));
  } finally {
    yield put(actionBuilder(DID_FETCH_USERS));
  }
}

export function* fetchUsersSaga() {
  yield takeEvery(FETCH_USERS, fetchUsers);
}
