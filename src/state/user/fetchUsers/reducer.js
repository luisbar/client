import {
  WILL_FETCH_USERS,
  FETCHING_USERS,
  DID_FETCH_USERS,
} from 'common/actions';

const initialState = {
  users: [],
  status: {
    isRunning: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export const fetchUsersReducer = (state = initialState, action) => {

  switch (action.type) {

    case WILL_FETCH_USERS:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: true,
          error: {
            errorMessage: '',
          },
        }
      };

    case FETCHING_USERS:
      if (action.error)
        return {
          ...state,
          status: {
            ...state.status,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        };

      return {
        ...state,
        users: action.payload,
        status: {
          ...state.status,
          wasASuccess: true,
        }
      };

    case DID_FETCH_USERS:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: false,
          wasAnError: false,
          wasASuccess: false,
        },
      };

    default:
      return state;
  }
};
