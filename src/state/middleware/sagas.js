import { all } from 'redux-saga/effects';

import { errorCatcherSaga as errorCatcher } from './errorCatcher';

export default function* root() {
  yield all([
    errorCatcher(),
  ]);
};
