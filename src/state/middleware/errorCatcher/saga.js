import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { SHOW_ERROR, SHOWING_ERROR } from 'common/actions';

function* errorCatcher({ payload }) {
  yield put(actionBuilder(SHOWING_ERROR, payload));
}

export function* errorCatcherSaga() {
  yield takeEvery(SHOW_ERROR, errorCatcher);
}
