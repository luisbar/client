import { SHOWING_ERROR } from 'common/actions';

const initialState = {
  errorMessage: '',
  timestamp: null,
};

export const errorCatcherReducer = (state = initialState, action) => {

  switch (action.type) {

    case SHOWING_ERROR:
      return {
        ...state,
        errorMessage: action.payload.errorMessage,
        timestamp: action.payload.timestamp,
      };

    default:
      return state;
  }
};
