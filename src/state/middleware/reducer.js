import { combineReducers } from 'redux';

import { errorCatcherReducer as errorCatcher } from './errorCatcher';

export default combineReducers({
  errorCatcher,
});
