import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { DELETE_SCOPE_URL } from 'common/config';
import { requester } from 'service/';
import {
  DELETE_SCOPE,
  WILL_DELETE_SCOPE,
  DELETING_SCOPE,
  DID_DELETE_SCOPE,
  DELETE_PRIVILEGE_FROM_SCOPE_PROPS,
} from 'common/actions';

function* deleteScope({ payload }) {
  try {
    yield put(actionBuilder(WILL_DELETE_SCOPE));
    const response = yield requester.get(DELETE_SCOPE_URL.replace(':id', payload.id));
    yield put(actionBuilder(DELETING_SCOPE, response, false));
    yield put(actionBuilder(DELETE_PRIVILEGE_FROM_SCOPE_PROPS, payload));
  } catch (error) {
    yield put(actionBuilder(DELETING_SCOPE, error, true));
  } finally {
    yield put(actionBuilder(DID_DELETE_SCOPE));
  }
}

export function* deleteScopeSaga() {
  yield takeEvery(DELETE_SCOPE, deleteScope);
}
