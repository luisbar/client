import {
  WILL_DELETE_SCOPE,
  DELETING_SCOPE,
  DID_DELETE_SCOPE,
} from 'common/actions';

const initialState = {
  status: {
    isRunning: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export const deleteScopeReducer = (state = initialState, action) => {

  switch (action.type) {

    case WILL_DELETE_SCOPE:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: true,
          error: {
            errorMessage: '',
          },
        }
      };

    case DELETING_SCOPE:
      if (action.error)
        return {
          ...state,
          status: {
            ...state.status,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        };

      return {
        ...state,
        status: {
          ...state.status,
          wasASuccess: true,
        }
      };

    case DID_DELETE_SCOPE:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: false,
          wasAnError: false,
          wasASuccess: false,
        },
      };

    default:
      return state;
  }
};
