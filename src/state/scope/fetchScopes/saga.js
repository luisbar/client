import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { FIND_ALL_SCOPES_URL } from 'common/config';
import { requester } from 'service/';
import {
  FETCH_SCOPES,
  WILL_FETCH_SCOPES,
  FETCHING_SCOPES,
  DID_FETCH_SCOPES
} from 'common/actions';

function* fetchScopes() {
  try {
    yield put(actionBuilder(WILL_FETCH_SCOPES));
    const response = yield requester.get(FIND_ALL_SCOPES_URL);
    yield put(actionBuilder(FETCHING_SCOPES, response, false));
  } catch (error) {
    yield put(actionBuilder(FETCHING_SCOPES, error, true));
  } finally {
    yield put(actionBuilder(DID_FETCH_SCOPES));
  }
}

export function* fetchScopesSaga() {
  yield takeEvery(FETCH_SCOPES, fetchScopes);
}
