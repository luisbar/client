import {
  WILL_FETCH_SCOPES,
  FETCHING_SCOPES,
  DID_FETCH_SCOPES,
  DELETE_PRIVILEGE_FROM_SCOPE_PROPS,
} from 'common/actions';

const initialState = {
  scopes: [],
  status: {
    isRunning: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export const fetchScopesReducer = (state = initialState, action) => {

  switch (action.type) {

    case WILL_FETCH_SCOPES:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: true,
          error: {
            errorMessage: '',
          },
        }
      };

    case FETCHING_SCOPES:
      if (action.error)
        return {
          ...state,
          status: {
            ...state.status,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        };

      return {
        ...state,
        scopes: action.payload,
        status: {
          ...state.status,
          wasASuccess: true,
        }
      };

    case DID_FETCH_SCOPES:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: false,
          wasAnError: false,
          wasASuccess: false,
        },
      };

    case DELETE_PRIVILEGE_FROM_SCOPE_PROPS:
      const privilegesFiltered = state.scopes.filter((item) => item.id !== action.payload.id);

      return {
        ...state,
        scopes: privilegesFiltered,
      };

    default:
      return state;
  }
};
