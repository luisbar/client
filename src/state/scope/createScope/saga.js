import { put, takeEvery } from 'redux-saga/effects'

import actionBuilder from 'common/actionBuilder';
import { CREATE_SCOPE_URL } from 'common/config';
import { requester } from 'service/';
import {
  CREATE_SCOPE,
  WILL_CREATE_SCOPE,
  CREATING_SCOPE,
  DID_CREATE_SCOPE
} from 'common/actions';

function* createScope({ payload }) {
  try {
    yield put(actionBuilder(WILL_CREATE_SCOPE));
    const response = yield requester.post(CREATE_SCOPE_URL, payload);
    yield put(actionBuilder(CREATING_SCOPE, response, false));
  } catch (error) {
    yield put(actionBuilder(CREATING_SCOPE, error, true));
  } finally {
    yield put(actionBuilder(DID_CREATE_SCOPE));
  }
}

export function* createScopeSaga() {
  yield takeEvery(CREATE_SCOPE, createScope);
}
