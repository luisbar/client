import {
  WILL_CREATE_SCOPE,
  CREATING_SCOPE,
  DID_CREATE_SCOPE,
} from 'common/actions';

const initialState = {
  status: {
    isRunning: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export const createScopeReducer = (state = initialState, action) => {

  switch (action.type) {

    case WILL_CREATE_SCOPE:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: true,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        }
      };

    case CREATING_SCOPE:
      if (action.error)
        return {
          ...state,
          status: {
            ...state.status,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.fieldMessages,
            },
          },
        };

      return {
        ...state,
        status: {
          ...state.status,
          wasASuccess: true,
        }
      };

    case DID_CREATE_SCOPE:
      return {
        ...state,
        status: {
          ...state.status,
          isRunning: false,
          wasAnError: false,
          wasASuccess: false,
        },
      };

    default:
      return state;
  }
};
