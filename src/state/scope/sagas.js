import { all } from 'redux-saga/effects';

import { fetchScopesSaga as fetchScopes } from './fetchScopes';
import { createScopeSaga as createScope } from './createScope';
import { deleteScopeSaga as deleteScope } from './deleteScope';

export default function* root() {
  yield all([
    fetchScopes(),
    createScope(),
    deleteScope(),
  ]);
};
