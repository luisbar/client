import { combineReducers } from 'redux';

import { fetchScopesReducer as fetchScopes } from './fetchScopes';
import { createScopeReducer as createScope } from './createScope';
import { deleteScopeReducer as deleteScope } from './deleteScope';

export default combineReducers({
  fetchScopes,
  createScope,
  deleteScope,
});
