import { combineReducers } from 'redux';

import { userReducer as user } from './user';
import { scopeReducer as scope } from './scope';
import { middlewareReducer as middleware } from './middleware';

export default combineReducers({
  user,
  scope,
  middleware,
});
