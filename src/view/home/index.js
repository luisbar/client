import React, { Component } from 'react';
import { Box, Heading, ResponsiveContext } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

class Home extends Component {

  render() {

    return (
      <Box
        fill>
        <ResponsiveContext.Consumer>
          {(size) => (
            <Heading
              alignSelf={'center'}
              truncate={true}>
              {size}
            </Heading>
          )}
        </ResponsiveContext.Consumer>
      </Box>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Home));
