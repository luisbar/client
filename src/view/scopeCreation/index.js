import React, { Component } from 'react';
import { Box, Button, Text } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { TXT_12, TXT_13, TXT_14 } from 'common/string';
import { CREATE_SCOPE } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { TextField } from 'component';

class ScopeCreation extends Component {

  state = {
    controller: '',
    handler: '',
  };

  render() {

    const { fieldErrorMessages } = this.props;

    return (
      <Box
        flex
        align={'center'}
        justify={'center'}>
        <Box
          width={'large'}>
          <TextField
            formField={{
              label: TXT_12,
              error: fieldErrorMessages.controller
            }}
            textInput={{
              plain: true,
              name: 'controller',
              onChange: this.onChange('controller'),
            }}/>
          <TextField
            formField={{
              label: TXT_13,
              error: fieldErrorMessages.handler
            }}
            textInput={{
              plain: true,
              name: 'handler',
              onChange: this.onChange('handler'),
            }}/>
          <Button
            primary
            onClick={this.onCreateScope}
            margin={{ top: 'small' }}
            label={
              <Text>
                {TXT_14}
              </Text>
            }/>
        </Box>
      </Box>
    );
  }

  onChange = (key) => (event) => {
    this.setState({ [key]:  event.target.value });
  }

  onCreateScope = () => {
    const { controller, handler } = this.state;

    this.props.createScope({ controller, handler });
  }
}

const mapStateToProps = state => {

  return {
    wasAnError: state
      .root
      .scope
      .createScope
      .status
      .wasAnError,
    fieldErrorMessages: state
      .root
      .scope
      .createScope
      .status
      .error
      .fieldErrorMessages,
  };
}

const mapDispatchToProps = dispatch => {

  return {
    createScope: (payload) => dispatch(actionBuilder(CREATE_SCOPE, payload)),
  }
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ScopeCreation));
