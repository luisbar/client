import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { Wrapper } from 'component';

class Main extends Component {

  render() {

    return (
      <Wrapper
        location={this.props.location}/>
    );
  }
}

const mapStateToProps = state => {

  return {
    state: state,
  };
};

const mapDispatchToProps = dispatch => {

  return {

  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Main));
