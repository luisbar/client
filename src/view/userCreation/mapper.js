export const mapScopesForShowingInSelectField = (scopes) => {

  return scopes.map((item) => ({
    id: item.id,
    label: `${item.controller}:${item.handler}`,
  }));
};

export const mapScopesForSavingAnUser = (scopes) => {

  return scopes.map((item) => ({
    id: item.id,
    controller: item.label.split(':')[0],
    handler: item.label.split(':')[1],
  }));
};
