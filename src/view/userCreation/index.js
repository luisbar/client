import React, { Component } from 'react';
import { Box, Button, Text } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { TextField, SelectField } from 'component';
import { TXT_20, TXT_21, TXT_22, TXT_23 } from 'common/string';
import { CREATE_USER, FETCH_SCOPES } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { mapScopesForShowingInSelectField, mapScopesForSavingAnUser } from './mapper';

class UserCreation extends Component {

  state = {
    username: '',
    password: '',
    scopes: [],
  }

  componentDidMount() {
    this.props.fetchScopes();
  }

  render() {

    let { scopes, wasASuccess, fieldErrorMessages } = this.props;

    if (wasASuccess)
      this.scopes = mapScopesForShowingInSelectField(scopes);

    return (
      <Box
        flex
        align={'center'}
        justify={'center'}>
        <Box
          width={'large'}>
          <TextField
            formField={{
              label: TXT_20,
              error: fieldErrorMessages.username
            }}
            textInput={{
              plain: true,
              name: 'username',
              onChange: this.onChange('username'),
            }}/>
          <TextField
            formField={{
              label: TXT_21,
              error: fieldErrorMessages.password
            }}
            textInput={{
              plain: true,
              name: 'password',
              onChange: this.onChange('password'),
            }}/>
          <SelectField
            options={this.scopes}
            placeholder={TXT_22}
            onSelectedItem={this.onSelectedPrivilege}
            onUnselectedItem={this.onUnselectedPrivilege}
            formField={{
              error: fieldErrorMessages.scopes,
              pad: { vertical: 'small' }
            }}
            select={{
              closeOnChange: false,
            }}/>

          <Button
            primary
            onClick={this.onCreateUser}
            margin={{ top: 'small' }}
            label={
              <Text>
                {TXT_23}
              </Text>
            }/>
        </Box>
      </Box>
    );
  }

  onChange = (key) => (event) => {
    this.setState({ [key]:  event.target.value });
  }

  onSelectedPrivilege = (privilege) => {
    this.setState({ scopes: this.state.scopes.concat([privilege]) });
  }

  onUnselectedPrivilege = (privilege) => {
    this.setState({ scopes: this.state.scopes.filter((item) => item.id !== privilege.id) });
  }

  onCreateUser = () => {
    const { username, password, scopes } = this.state;
    this.props.createUser({ username, password, scopes: mapScopesForSavingAnUser(scopes) });
  }
}

const mapStateToProps = state => {

  return {
    scopes: state
      .root
      .scope
      .fetchScopes
      .scopes,
    wasASuccess: state
      .root
      .scope
      .fetchScopes
      .status
      .wasASuccess,
    wasAnError: state
      .root
      .user
      .createUser
      .status
      .wasAnError,
    fieldErrorMessages: state
      .root
      .user
      .createUser
      .status
      .error
      .fieldErrorMessages,
  };
}

const mapDispatchToProps = dispatch => {

  return {
    fetchScopes: () => dispatch(actionBuilder(FETCH_SCOPES)),
    createUser: (payload) => dispatch(actionBuilder(CREATE_USER, payload)),
  }
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(UserCreation));
