import React, { Component } from 'react';
import { Box, Heading } from 'grommet';

import { TXT_3 } from 'common/string';

class NotFound extends Component {

  render() {

    return (
      <Box
        flex={true}
        justify={'center'}
        align={'center'}
        colorIndex={'accent-1'}>
        <Heading>
          {TXT_3}
        </Heading>
      </Box>
    );
  }
}

export default NotFound;
