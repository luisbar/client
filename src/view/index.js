export { default as Home }  from 'view/home';
export { default as Login } from 'view/login';
export { default as NotFound } from 'view/notFound';
export { default as User } from 'view/user';
export { default as UserCreation } from 'view/userCreation';
export { default as Scope } from 'view/scope';
export { default as ScopeCreation } from 'view/scopeCreation';
