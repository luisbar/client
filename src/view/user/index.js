import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import actionBuilder from 'common/actionBuilder';
import { FETCH_USERS } from 'common/actions';
import { USER_UPSERT } from 'common/config';
import { DataTable } from 'component';

class User extends Component {

  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {

    return (
      <DataTable
        data={this.props.users}
        headerLabels={['id', 'usuario']}
        dataKeys={['id', 'username']}
        floatingButtonPath={USER_UPSERT}
        onEdit={this.onEdit}
        onView={this.onView}
        onDelete={this.onDelete}/>
    );
  }

  onEdit = (id) => {
    console.log('Edit', id);
  }

  onView = (id) => {
    console.log('View', id);
  }

  onDelete = (id) => {
    console.log('Delete', id);
  }
}

const mapStateToProps = state => {

  return {
    users: state
      .root
      .user
      .fetchUsers
      .users,
  };
};

const mapDispatchToProps = dispatch => {

  return {
    fetchUsers: () => dispatch(actionBuilder(FETCH_USERS)),
  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(User));
