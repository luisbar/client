import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Box } from 'grommet';

import actionBuilder from 'common/actionBuilder';
import { FETCH_SCOPES, DELETE_SCOPE } from 'common/actions';
import { SCOPE_CREATE } from 'common/config';
import { DataTable, ConfirmModal } from 'component';
import { TXT_15, TXT_16, TXT_17, TXT_18, TXT_19 } from 'common/string';

class Scope extends Component {

  state = {
    confirmModalVisibility: false,
  }

  componentDidMount() {
    this.props.fetchScopes();
  }

  render() {

    const { scopes } = this.props;
    const { confirmModalVisibility, id } = this.state;

    return (
      <Box
        fill>
        <DataTable
          data={scopes}
          headerLabels={[TXT_15, TXT_16, TXT_17]}
          dataKeys={['id', 'controller', 'handler']}
          floatingButtonPath={SCOPE_CREATE}
          onDelete={this.onDelete}/>
        <ConfirmModal
          title={TXT_18}
          message={TXT_19.replace(':id', id)}
          visibility={confirmModalVisibility}
          onCancel={this.onCancel}
          onAccept={this.onAccept}/>
      </Box>
    );
  }

  onDelete = (id) => {
    this.setState({ confirmModalVisibility: true, id });
  }

  onCancel = () => {
    this.setState({ confirmModalVisibility: false });
  }

  onAccept = () => {
    this.props.deleteScope({ id: this.state.id });
    this.setState({ confirmModalVisibility: false });
  }
}

const mapStateToProps = state => ({
  scopes: state
    .root
    .scope
    .fetchScopes
    .scopes,
});

const mapDispatchToProps = dispatch => ({
  fetchScopes: () => dispatch(actionBuilder(FETCH_SCOPES)),
  deleteScope: (payload) => dispatch(actionBuilder(DELETE_SCOPE, payload)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Scope));
