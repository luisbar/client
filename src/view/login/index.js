import React, { Component } from 'react';
import { Box, Button, Text } from 'grommet';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';

import { TXT_6 } from 'common/string';
import { HOME, AUTHORIZE_URL } from 'common/config';
import generateState from 'common/generateState';

class Login extends Component {

  render() {

    return (
      <Box
        fill
        justify={'center'}
        align={'center'}
        background={'dark-3'}>

        <Button
          primary
          label={<Text color={'dark-1'}>{TXT_6}</Text>}
          href={AUTHORIZE_URL.replace('STATE', generateState(16))}/>
      </Box>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  goToHome: () => dispatch(replace(HOME)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login));
