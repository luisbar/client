import { base } from 'grommet/themes';

export default {
  ...base,
  global: {
    ...base.global,
    colors: {
      ...base.global.colors,
    },
    breakpoints: {
      ...base.global.breakpoints,
      xsmall: {
        value: 200
      },
    }
  },
  text: {
    extend: {
      fontFamily: 'Quicksand, sans-serif',
    }
  },
  heading: {
    ...base.heading,
    font: {
      family: 'Quicksand, sans-serif',
    },
  },
  paragraph: {
    extend: {
      fontFamily: 'Quicksand, sans-serif',
    }
  },
  select: {
    control: {
      extend: {
        border: '1px solid white'
      },
    },
  },
};
