import { SHOW_ERROR } from 'common/actions';

export default (store) => (next) => (action) => {

  if (action.error && action.payload.message)
    store.dispatch({
      type: SHOW_ERROR,
      payload: {
        errorMessage: action.payload.message,
        timestamp: new Date().getTime(),
      },
    });

  next(action);

};
