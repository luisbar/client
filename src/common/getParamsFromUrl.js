const getParamsFromUrl = () => {
  const url = window.location.href;
  const queryString = url.split('?')[1];
  const rawParams = typeof queryString === 'string' && queryString.length > 0 ?
    queryString.split('&') : '';
  const params = {};

  for (let i = 0; i < rawParams.length; i++) {
    const rawParam = rawParams[i];
    const param = rawParam.split('=');
    let paramName = param[0];
    const paramValue = param[1];

    if (paramName.indexOf('[]') === -1) {
      // Handle literal params
      if (paramValue && paramValue !== '') {
        params[paramName] = decodeURIComponent(paramValue);
      }
    }
    else {
      // Handle array params
      paramName = paramName.replace(/[[|\]]/g, '');

      if (params[paramName] === undefined) {
        params[paramName] = [];
      }

      params[paramName].push(decodeURIComponent(paramValue));
    }
  }

  return Object.keys(params).length > 0 ? params : null;
}

export default getParamsFromUrl;
