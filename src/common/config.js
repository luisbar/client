//View ids
export const HOME = '/';
export const LOGIN = '/login';
export const USER = '/user';
export const SCOPE = '/scope';
export const CLIENT = '/client';
export const USER_UPSERT = '/user/upsert';
export const SCOPE_CREATE = '/scope/create';

//Oauth server data
export const RESPONSE_TYPE = 'token';
export const CLIENT_ID = '123abc';
export const REDIRECT_URI = 'http%3A%2F%2Flocalhost%3A3000';
export const SCOPES = [
  'user:upsert',
  'user:findAll',
  'user:findById',
  'user:delete',
  'scope:create',
  'scope:findAll',
  'scope:delete',
];

//Endpoints
export const OAUTH_SERVER_URL = 'http://localhost:3001';
export const AUTHORIZE_URL = `${OAUTH_SERVER_URL}/authorize?response_type=${RESPONSE_TYPE}&scope=${SCOPES.join(' ')}&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&state=STATE`;
export const FIND_ALL_USERS_URL = `${OAUTH_SERVER_URL}/user/find`;
export const UPSERT_USER_URL = `${OAUTH_SERVER_URL}/user/upsert`;
export const FIND_ALL_SCOPES_URL = `${OAUTH_SERVER_URL}/scope/find`;
export const CREATE_SCOPE_URL = `${OAUTH_SERVER_URL}/scope/create`;
export const DELETE_SCOPE_URL = `${OAUTH_SERVER_URL}/scope/delete/:id`;
