//ErrorBoundary
const TXT_1 = 'Hubo un problema, lo sentimos si tienes algún inconveniente con la página.';
const TXT_2 = 'Detalle';

//NotFound
const TXT_3 = 'No se encontro el recurso :(';

//Home
const TXT_4 = 'Login';
const TXT_5 = 'FETCH POKEMONS';

//Login
const TXT_6 = 'Entrar';

//SideMenu
const TXT_7 = 'Inicio';
const TXT_8 = 'Usuario';
const TXT_9 = 'Alcance';
const TXT_10 = 'Cliente';

//SignOutItem
const TXT_11 = 'Salir';

//ScopeCreation
const TXT_12 = 'Controlador';
const TXT_13 = 'Manejador';
const TXT_14 = 'Guardar';

//Scope
const TXT_15 = 'id';
const TXT_16 = 'controlador';
const TXT_17 = 'manejador';
const TXT_18 = 'Confirmación';
const TXT_19 = '¿Esta seguro(a) que desea eliminar el privilegio con indetificador :id?';

//UserCreation
const TXT_20 = 'Nombre de usuario';
const TXT_21 = 'Contraseña';
const TXT_22 = 'Seleccione privilegio(s)';
const TXT_23 = 'Guardar';

export {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
  TXT_14,
  TXT_15,
  TXT_16,
  TXT_17,
  TXT_18,
  TXT_19,
  TXT_20,
  TXT_21,
  TXT_22,
  TXT_23,
};
