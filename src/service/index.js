import Requester from './requester';

const axios = require('axios');

export const requester = new Requester(axios.create({
  timeout: 10000,
}));
