import { sessionService } from 'redux-react-session';
const debug = require('debug')('service:requester');

export default class Requester {

  constructor(fetch) {
    this.fetch = fetch;
  }

  async get(url, params) {

    debug('url: ', url)
    debug('params: ', params)

    const session = await sessionService.loadSession();

    return this.fetch({ url, params, method: 'GET', headers: { Authorization: `Bearer ${session.accessToken}` } })
      .then(this.errorHandler)
      .then((data) => data)
      .catch((error) => {
        debug('error: ', error.response.data)
        throw error.response.data
      });
  }

  async post(url, data) {

    debug('url: ', url)
    debug('data: ', data)

    const session = await sessionService.loadSession();

    return this.fetch({ url, data, method: 'POST', headers: { Authorization: `Bearer ${session.accessToken}` } })
      .then(this.errorHandler)
      .then((data) => data)
      .catch((error) => {
        debug('error: ', error.response.data)
        throw error.response.data
      });
  }

  errorHandler(response) {

    debug('errorHandler: ', response)

    switch (response.status) {

      case 200:
      case 201:
        return response.data;

      default:
        return response.data;
    }
  }
}
